module gitlab.com/diycoder/protoc-gen-micro

go 1.14

require (
	github.com/golang/protobuf v1.5.2
	gitlab.com/diycoder/go-micro v0.0.1
	google.golang.org/genproto v0.0.0-20210701191553-46259e63a0a9
)
